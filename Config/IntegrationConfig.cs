using System;

namespace PessoaCrud.Config{

    public class IntegrationConfig {

        public string Key { get; set; }
        public string Description {get; set;}
		
		public string Fields {get; set;}
		public string DataOperations { get; set; }
		public string FilterLinqExpression {get; set;}
		
		public string Type { get; set; }
		public string Format {get; set;}
        
		public string EmailToTemplate {get; set;}
		public string EmailSubjectTemplate {get; set;}
		public string EmailBodyTemplate {get; set;}
		public string EmailBodyHtmlTemplate {get; set;}
		
		public string RequestUrl {get; set;}
		public string RequestMethod {get; set;}

    }

}