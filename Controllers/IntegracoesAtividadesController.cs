using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PessoaCrud.Data;
using PessoaCrud.Models;

namespace PessoaCrud.Controllers
{
    public class IntegracoesAtividadesController : Controller
    {
        private readonly StorageContext _context;

        public IntegracoesAtividadesController(StorageContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DescSortParm"] = String.IsNullOrEmpty(sortOrder) ? "desc_desc" : "";
            ViewData["EntSortParm"] = sortOrder == "Date" ? "ent_desc" : "ent_asc";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var queryable = from s in _context.AtividadesIntegracoes
                           select s;

            queryable.Include(e => e.Integracao);

            if (!String.IsNullOrEmpty(searchString))
            {
                queryable = queryable.Where(e => e.Integracao.Descricao.Contains(searchString)
                                       || e.NomeEntidade.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "desc_desc":
                    queryable = queryable.OrderByDescending(e => e.Integracao.Descricao);
                    break;
                case "ent_asc":
                    queryable = queryable.OrderBy(s => s.NomeEntidade);
                    break;
                case "ent_desc":
                    queryable = queryable.OrderByDescending(s => s.NomeEntidade);
                    break;
                default:
                    queryable = queryable.OrderByDescending(s => s.Criacao);
                    break;
            }

            int pageSize = 10;
            return View(await PaginatedList<IntegracaoAtividade>.CreateAsync(queryable.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _context.AtividadesIntegracoes
            
                 .AsNoTracking()
                 .Include(e => e.Integracao)
                 .FirstOrDefaultAsync(m => m.ID == id);

            if (entity == null)
            {
                return NotFound();
            }

            return View(entity);
        }

    }
}