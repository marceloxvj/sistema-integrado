using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PessoaCrud.Data;
using PessoaCrud.Models;

namespace PessoaCrud.Controllers
{
    public class IntegracoesController : Controller
    {
        private readonly StorageContext _context;

        public IntegracoesController(StorageContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["TypeSortParm"] = sortOrder == "Date" ? "type_desc" : "type_asc";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var queryable = from s in _context.Integracoes
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                queryable = queryable.Where(e => e.Descricao.Contains(searchString)
                                       || e.Chave.Contains(searchString)
                                       || e.EmailTemplateCorpoHtml.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    queryable = queryable.OrderByDescending(e => e.Descricao);
                    break;
                case "type_asc":
                    queryable = queryable.OrderBy(s => s.Tipo);
                    break;
                case "type_desc":
                    queryable = queryable.OrderByDescending(s => s.Tipo);
                    break;
                default:
                    queryable = queryable.OrderBy(s => s.Descricao);
                    break;
            }

            int pageSize = 3;
            return View(await PaginatedList<Integracao>.CreateAsync(queryable.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _context.Integracoes
            
                 .AsNoTracking()
                 .FirstOrDefaultAsync(m => m.ID == id);

            if (entity == null)
            {
                return NotFound();
            }

            return View(entity);
        }

    }
}