using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PessoaCrud.Data;
using PessoaCrud.Models;

namespace PessoaCrud.Controllers
{
    public class PessoasController : Controller
    {
        private readonly StorageContext _context;

        public PessoasController(StorageContext context)
        {
            _context = context;
        }

        // GET: Pessoas
        public async Task<IActionResult> Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["EmailSortParm"] = sortOrder == "Date" ? "email_desc" : "email_asc";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var queryable = from s in _context.Pessoas
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                queryable = queryable.Where(e => e.Nome.Contains(searchString)
                                       || e.Email.Contains(searchString)
                                       || e.Telefone.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    queryable = queryable.OrderByDescending(e => e.Nome);
                    break;
                case "email_asc":
                    queryable = queryable.OrderBy(s => s.Email);
                    break;
                case "date_desc":
                    queryable = queryable.OrderByDescending(s => s.Email);
                    break;
                default:
                    queryable = queryable.OrderBy(s => s.Nome);
                    break;
            }

            int pageSize = 3;
            return View(await PaginatedList<Pessoa>.CreateAsync(queryable.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Pessoas/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _context.Pessoas
                 .AsNoTracking()
                 .FirstOrDefaultAsync(m => m.ID == id);

            if (entity == null)
            {
                return NotFound();
            }

            return View(entity);
        }

        // GET: Pessoas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Pessoas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Nome,Idade,Email,Telefone")] Pessoa entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(entity);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(entity);
        }

        // GET: Pessoas/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _context.Pessoas.FindAsync(id);
            if (entity == null)
            {
                return NotFound();
            }
            return View(entity);
        }

        // POST: Pessoas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var entityToUpdate = await _context.Pessoas.FirstOrDefaultAsync(s => s.ID == id);
            if (await TryUpdateModelAsync<Pessoa>(
                entityToUpdate,
                "",
                s => s.Nome, s => s.Idade, s => s.Email, s => s.Telefone))
            {
                try
                {
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(entityToUpdate);
        }

        // GET: Pessoas/Delete/5
        public async Task<IActionResult> Delete(long? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _context.Pessoas
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.ID == id);
            if (entity == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "see your system administrator.";
            }

            return View(entity);
        }
        // POST: Pessoas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var entity = await _context.Pessoas.FindAsync(id);
            if (entity == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.Pessoas.Remove(entity);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.)
                return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
            }
        }

        private bool StudentExists(int id)
        {
            return _context.Pessoas.Any(e => e.ID == id);
        }

    }
}