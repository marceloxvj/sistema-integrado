﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PessoaCrud.Config;
using PessoaCrud.Models;

namespace PessoaCrud.Data
{
    public static class DbInitializer
    {
        public static void Initialize(StorageContext context, IConfiguration configuration)
        {
            //context.Database.EnsureCreated();

            List<IntegrationConfig> integrations = configuration.GetSection("Integrations").Get<List<IntegrationConfig>>();

            foreach(IntegrationConfig config in integrations){
                
                var integracao = context.Integracoes.FirstOrDefault(i => i.Chave == config.Key);

                if(integracao == null){
                    integracao = new Integracao();
                    context.Add(integracao);
                }

                Integracao.FromConfig(config, integracao);
            }
            context.SaveChanges();

            if(!context.Pessoas.Any()){

                var pessoas = new Pessoa[]
                {
                    new Pessoa{ Nome="Cleber Zanella", Idade=80, Email="cleber.zanella@inventti.com.br", Telefone="47999556633" },
                    new Pessoa{ Nome="Joao", Idade=80, Email="joao@outlook.com", Telefone="47999556633" }
                };

                foreach(var pessoa in pessoas){
                    context.Pessoas.Add(pessoa);
                }

                context.SaveChanges();
            }

        }
    }
}
