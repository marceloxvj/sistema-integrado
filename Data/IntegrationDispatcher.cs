using System;
using System.Collections.Generic;
using PessoaCrud.Data;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PessoaCrud.Models;
using PessoaCrud.Services;

namespace PessoaCrud.Data {

    public class IntegrationDispatcher {

        private readonly ILogger<IntegrationDispatcher> _logger;
        private readonly Mailing _mailerService;
        private readonly WebCall _webcallService;

        public IntegrationDispatcher(ILoggerFactory loggerFactory, Mailing mailerService, WebCall webcallService){
            _logger = loggerFactory.CreateLogger<IntegrationDispatcher>();
            _mailerService = mailerService;
            _webcallService = webcallService;
        }

        public List<IntegracaoAtividade> Dispatch(StorageContext storageContext, EntityEntry entry){

            if(typeof(Pessoa) != entry.Entity.GetType()){
                return new List<IntegracaoAtividade>();
            }

            var atividades = new List<IntegracaoAtividade>();
            var integracoes = storageContext.Integracoes.AsNoTracking();
            foreach(var integracao in integracoes){

                if(integracao.Operacoes == null){
                    _logger.LogWarning("Integração {0} não tem operações declaradas.", integracao.Chave);
                    continue;
                }

                if(!integracao.Operacoes.Contains(entry.State.ToString())){
                    continue;
                }

                if(integracao.ExpressaoFiltro != null){
                    string errorMessage = DynamicLinq.VerifyUnaryExprError<Pessoa>(integracao.ExpressaoFiltro);
                    if(errorMessage != null){
                        _logger.LogWarning("Integração {0} com expressão de filtro incorreta: {1}.", integracao.Chave, errorMessage);
                        continue;
                    } else {
                        if(!DynamicLinq.EvaluateUnaryExprError<Pessoa>((Pessoa) entry.Entity, integracao.ExpressaoFiltro)){
                            _logger.LogDebug("Integração {0} entidade não passou na expressão de filtro: {1}.", integracao.Chave, entry.Entity);
                            continue;    
                        }
                    }
                }

                try {

                    if(integracao.Tipo == TipoIntegracao.Email){
                        atividades.Add(DispatchEmail(storageContext, integracao, entry));
                        continue;    
                    }

                    if(integracao.Tipo == TipoIntegracao.WebCall){
                        atividades.Add(DispatchWebCall(storageContext, integracao, entry));
                        continue;    
                    }

                } catch(Exception exc){
                    _logger.LogError(exc, "Erro ao executar integração {0}.", integracao.Chave);
                }

            }

            return atividades;
        }

        IntegracaoAtividade DispatchEmail(StorageContext storageContext, Integracao integracao, EntityEntry entry){

            _logger.LogInformation(
                "Email integration Changed {0} from {1} to {2}.",
                entry.Entity.GetType().Name, entry.OriginalValues, entry.CurrentValues);

            return _mailerService.SendIntegration(storageContext, integracao, entry);

        }

        IntegracaoAtividade DispatchWebCall(StorageContext storageContext, Integracao integracao, EntityEntry entry){

            _logger.LogInformation(
                "WebCall integration Changed {0} from {1} to {2}.",
                entry.Entity.GetType().Name, entry.OriginalValues, entry.CurrentValues);
            
            return _webcallService.SendIntegration(storageContext, integracao, entry);

        }

    }

}