﻿using System;
using System.Collections.Generic;
using PessoaCrud.Models;
using PessoaCrud.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace PessoaCrud.Data
{
    public class StorageContext : DbContext
    {
        private IntegrationDispatcher _dispatcher;

        public StorageContext(DbContextOptions<StorageContext> options, IntegrationDispatcher dispatcher) : base(options)
        {
            this._dispatcher = dispatcher;
        }

        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Integracao> Integracoes { get; set; }

        public DbSet<IntegracaoAtividade> AtividadesIntegracoes { get; set; }

        public override int SaveChanges(){

            var activities = new List<IntegracaoAtividade>();

            foreach(var entry in this.ChangeTracker.Entries()){

                if(entry.State != EntityState.Added && entry.State != EntityState.Modified && entry.State != EntityState.Deleted){
                    continue;
                }

                activities.AddRange(_dispatcher.Dispatch(this, entry));
            }

            int result = base.SaveChanges();

            if(activities.Count > 0){
                AtividadesIntegracoes.AddRange(activities);
                base.SaveChanges();
            }
            
            return result;
        }

        public int InnerSaveChanges(){
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Pessoa>().ToTable("Pessoas");
        }
    }
}