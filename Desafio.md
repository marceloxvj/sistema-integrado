## Desafio

Desafio consiste em completar 3 itens:

1 - Fazer deploy automatizado da aplicação PessoaCrud, rodando em docker no Heroku (ou qualquer outro ambiente de nuvem gratuito que você conheça). O resultado esperado é apenas executar um script do tipo "build-and-deploy-at-heroku.sh <heroku-app-name>" passando poucos parâmetros e ter como resultado o sistema rodando no heroku.
Exemplo rodando: https://sistema-integrado.herokuapp.com/

2 - Documentar a solução criada indicando o passo a passo de deploy no README.md e guardando script criado dentro da pasta "deploy/heroku" ou "deploy/gcp" deste próprio reposítório Git.

3 - Explicar textualmente como você faria para escalar uma aplicação de um para três nós usando docker. Esperamos de você uma explicação bastante técnica e factível, citando o nome dos componentes que você utilizaria para resolver este problema.


### Avaliação

* Link da aplicação rodando ao vivo. Solicitamos a utilização de um Cloud gratuito justamente para você poder nos mostrar seu deploy rodando
* Verificaremos se o seu script realmente funciona, seguindo a documentação no README.md.
* Verificaremos se você guardou seu script no git
* Verificaremos se você foi capaz de escrever um script simples e bem documentado
* Faremos a análise da solução descrita para escalar um aplicação docker. 

### Dicas

* Ver o que empresas como Google, RedHat, Pivotal e [ThoughtWorks](https://www.thoughtworks.com/radar) andam testando
* Na Inventti nós usamos deploy on premise, tente se preocupar com isso ao montar a solução
* Seria muito legal ver como entrega extra um CI com Jenkins, GitLab ou algo assim

### Entrega

O prazo de entrega padrão é uma semana após o envio do desafio por email
As alterações realizadas após este prazo não serão avaliadas
Guarde sua solução neste repositório do Git
Qualquer dúvida, entre em contato pelo email: michel@inventti.com.br
