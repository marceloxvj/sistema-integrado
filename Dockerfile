# Construindo a imagem de build 
FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build-env
WORKDIR /app

# Copiando o arquivo .csproj
COPY *.csproj ./
RUN dotnet restore

# Copiando os fontes e construindo
COPY . ./
RUN dotnet publish -c Release -o out

# Construindo imagem de runtime
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0
ENV SEND_GRID_API_KEY = "B2tAet3LRjG7QpKf2EJYnQ"
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "PessoaCrud.dll"]





