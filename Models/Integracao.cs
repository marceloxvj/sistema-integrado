using System;
using PessoaCrud.Config;
using PessoaCrud.Models.Common;

namespace PessoaCrud.Models
{
    public enum TipoIntegracao
    {
        Email,WebCall
    }

    public enum IntegracaoFormato
    {
        Json,Xml,Csv
    }

    public class Integracao : EntityBase<long>
    {
        public string Chave { set; get; }
        public string Descricao { set; get; }

        public string Campos { set; get; }
        public string Operacoes { set; get; }
        public string ExpressaoFiltro { set; get; }


        public TipoIntegracao Tipo { set; get; }
        public IntegracaoFormato? Formato { set; get; }

        public string EmailTemplateDestinatarios { get; set; }
        public string EmailTemplateAssunto { get; set; }
        public string EmailTemplateCorpo { get; set; }
        public string EmailTemplateCorpoHtml { get; set; }

        public string RequisicaoUrl { get; set; }
        public string RequisicaoMetodo { get; set; }

        public static void FromConfig(IntegrationConfig config, Integracao integracao){

            integracao.Chave = config.Key;
            integracao.Descricao = config.Description;

            integracao.Campos = config.Fields;
            integracao.Operacoes = config.DataOperations;
            integracao.ExpressaoFiltro = config.FilterLinqExpression;

            switch(config.Type){
                case "WebCall":
                    integracao.Tipo = TipoIntegracao.WebCall;
                    break;
                case "Email":
                    integracao.Tipo = TipoIntegracao.Email;
                    break;
            }

            switch(config.Format){
                case "xml":
                    integracao.Formato = IntegracaoFormato.Xml;
                    break;
                case "json":
                    integracao.Formato = IntegracaoFormato.Json;
                    break;
                case "csv":
                    integracao.Formato = IntegracaoFormato.Csv;
                    break;
            }

            integracao.EmailTemplateDestinatarios = config.EmailToTemplate;
            integracao.EmailTemplateAssunto = config.EmailSubjectTemplate;
            integracao.EmailTemplateCorpoHtml = config.EmailBodyHtmlTemplate;
            
            integracao.RequisicaoUrl = config.RequestUrl;
            integracao.RequisicaoMetodo = config.RequestMethod;

        }


    }
}