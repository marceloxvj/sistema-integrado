using System;
using PessoaCrud.Models.Common;

namespace PessoaCrud.Models
{
    public class IntegracaoAtividade : EntityBase<long>
    {
        public DateTime? Criacao {get; set;}

        public long IntegracaoID { set; get; }
        public Integracao Integracao { get; set; }

        public int HttpStatus {get;set;}
        public string HttpResponse {get;set;}
        public string HttpSentData {get;set;}

        public string NomeEntidade {get;set;}
        public long IdentificadorEntidade {get;set;}

        public string SendGridEmailId { set; get; }

    }

}