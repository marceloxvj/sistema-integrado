using System;
using PessoaCrud.Models.Common;

namespace PessoaCrud.Models
{
    public class Pessoa : EntityBase<long>
    {
        public string Nome { set; get; }
        public int Idade { get; set; }
        public string Telefone { set; get; }
        public string Email { set; get; }

    }

}