# Desafio.DevOps.Exemplo

## O que é

Aplicação CRUD Asp.net Core com deploy simplificado para atestar o conhecimento sobre docker, nuvem, CI e CD de candidatos a DevOps.

Maiores detalhes sobre o desafio estão em [Desafio.md](/Desafio.md).

## Pré-requisitos para compilação e execução

* Dot Net Core Runtime 3.x (para executar a aplicação após compilação, implantação)
* [Dot Net Core SDK 3.x](https://dotnet.microsoft.com/download) (para ambiente de desenvolvimento)

## Compilar

- Build simples.
```
# considerando que o Dot Net Core SDK já esteja instalado
# ir até a pasta dos fontes
dotnet build
```
- Executar a partir dos fontes.
```
# configurar porta do servidor web e API Key do Send Grid
export PORT=5005
export SEND_GRID_API_KEY="SG.xxxxxxxxxxxxxxxxxxxxxx.k_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

dotnet run
```
- Build do pacote para deploy em linux 64bits.
```
dotnet publish -r linux-x64 -c Release /p:PublishSingleFile=true
# gerará os arquivos em: bin/Release/netcoreapp3.0/linux-x64/publish
```
- Executar a partir do pacote compilado
```
# Copiar arquivos da aplicação:
# copiar para pasta de execução arquivos compilados em "bin/Release/netcoreapp3.0/linux-x64/publish" para pasta de execução
# copiar  para pasta de execução arquivo de configuração "appsettings.json" com as definições de integrações.

# configurar porta do servidor web e API Key do Send Grid
export PORT=5005
export SEND_GRID_API_KEY="SG.xxxxxxxxxxxxxxxxxxxxxx.k_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

chmod +x PessoaCrud
./PessoaCrud
```

## Deploy

Instruções para deploy automatizado em ambientes cloud.

### Heroku

- Instalar [docker cli](https://docs.docker.com/engine/reference/commandline/cli/)
- Instalar [Heroku cli](https://devcenter.heroku.com/articles/heroku-cli).
- Habilitar o Docker container na conta do Heroku
- Executar o scrip de deploy
```
# demais instruçes específicas de deploy no heroku
# ....
```

### Google Cloud (CGP)

- Instruções específicas para CGP quando for solicitado deploy para esse ambiente

### Azure

- Instruçes específicas para Azure quando for solicitado deploy para esse ambiente
