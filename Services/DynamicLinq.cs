using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Linq.Dynamic.Core.Exceptions;

namespace PessoaCrud.Services {

    public class DynamicLinq {

        public static string VerifyUnaryExprError<T>(string lambdaExpression){

            var param1 = Expression.Parameter(typeof(T), typeof(T).Name);

            try {
                DynamicExpressionParser.ParseLambda(new[] { param1 }, null, lambdaExpression); 
            } catch(ParseException exc){
              return exc.Message;  
            }

            return null;
        }

        public static bool EvaluateUnaryExprError<T>(T entity, string lambdaExpression){

            var param1 = Expression.Parameter(typeof(T), typeof(T).Name);

            LambdaExpression e;
            try {
                e = DynamicExpressionParser.ParseLambda(new[] { param1 }, null, lambdaExpression); 
            } catch(ParseException){
              return false;  
            }

            return (bool) e.Compile().DynamicInvoke(entity);
        }

    }

}