using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Xml.Linq;
using PessoaCrud.Models;
using PessoaCrud.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using HandlebarsDotNet;
using System.ComponentModel;
using Newtonsoft.Json;

namespace PessoaCrud.Services 
{

    public class Formats 
    {

        public static void FilterFields(IDictionary<string, object> values, string fields){

            var fieldList = new List<string>(fields.Split(","));

            foreach(string propertyName in new List<string>(values.Keys)){
                if(!fieldList.Contains(propertyName)){
                    values.Remove(propertyName);
                }
            }
            
        }


        public static string ToXml(IDictionary<string, object> values, string rootElement){

            XElement el = new XElement(rootElement,
                values.Select(kv => new XElement(kv.Key, kv.Value)));

            return el.ToString();
        }

        public static string ToCsv(IDictionary<string, object> values){

            string csvHeader = string.Join(
                ",",
                values.Select(d => d.Key )
            );

            string csv = string.Join(
                ",",
                values.Select(d => d.Value)
            );

            return csvHeader + Environment.NewLine + csv;
        }

        public static string ToJson(IDictionary<string, object> values){

            return JsonConvert.SerializeObject(values);
        }

    }
}