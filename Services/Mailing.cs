using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using PessoaCrud.Models;
using PessoaCrud.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace PessoaCrud.Services {

    public class Mailing {

        private static readonly string FromAddress = "no-reply@sistemaintegrado.com.br";
        private string _apiKey;
        public Mailing(string apiKey){
            this._apiKey = apiKey;
        }

        public IntegracaoAtividade SendIntegration(StorageContext storageContext, Integracao integracao, EntityEntry entry)
        {
            IDictionary<string, object> state = Templating.ObjectToDictionary(entry.Entity);

            long id = (long) state["ID"];

            var client = new SendGridClient(_apiKey);
            var from = new EmailAddress(FromAddress);
            var subject = Templating.Parse(integracao.EmailTemplateAssunto, state);
            var to = new EmailAddress(Templating.Parse(integracao.EmailTemplateDestinatarios, state));
            var htmlContent = Templating.Parse(integracao.EmailTemplateCorpoHtml, state);

            var plainTextContent = integracao.EmailTemplateCorpo == null ? htmlContent : Templating.Parse(integracao.EmailTemplateCorpo, state);

            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            if(integracao.Formato != null){

                if(integracao.Campos != null){
                    Formats.FilterFields(state, integracao.Campos);
                }

                string fileName = null;
                string content = null;

                if(integracao.Formato == IntegracaoFormato.Csv){
                    fileName = "dados.csv";
                    content = Formats.ToCsv(state);
                } else

                if(integracao.Formato == IntegracaoFormato.Xml){
                    fileName = "dados.xml";
                    content = Formats.ToXml(state, entry.Entity.GetType().Name);
                } else

                if(integracao.Formato == IntegracaoFormato.Json){
                    fileName = "dados.json";
                    content = Formats.ToJson(state);
                }

                byte[] byteData = Encoding.ASCII.GetBytes(content);
                msg.AddAttachment(fileName, Convert.ToBase64String(byteData));

            }

            var response = client.SendEmailAsync(msg).GetAwaiter().GetResult();

            string messageId = null;
            if(response.Headers.Contains("X-Message-ID")){
                var values = new List<string>(response.Headers.GetValues("X-Message-ID"));
                messageId = values[0];
            }

            return new IntegracaoAtividade()
            {
                IntegracaoID = integracao.ID,
                Criacao = DateTime.Now,
                NomeEntidade = entry.Entity.GetType().Name,
                IdentificadorEntidade = id,

                HttpStatus = (int) response.StatusCode,
                HttpSentData = Formats.ToJson(state),
                SendGridEmailId = messageId

            };

        }

    }

}