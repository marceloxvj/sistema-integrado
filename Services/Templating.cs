using System.Collections.Generic;
using System.Net.Http;
using PessoaCrud.Models;
using PessoaCrud.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using HandlebarsDotNet;
using System.ComponentModel;

namespace PessoaCrud.Services 
{

    public class Templating 
    {

        public static IDictionary<string, object> ObjectToDictionary(object source)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source)){
                dictionary.Add(property.Name, property.GetValue(source));
            }

            return dictionary;
        }

        public static string Parse(string templateStr, object data)
        {
            var template = Handlebars.Compile(templateStr);
            return template(data);
        }

    }

}