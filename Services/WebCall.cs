using System;
using System.Web;
using System.Collections.Generic;
using System.Net.Http;
using PessoaCrud.Models;
using PessoaCrud.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace PessoaCrud.Services {

    public class WebCall {

        private readonly HttpClient client = new HttpClient();

        public IntegracaoAtividade SendIntegration(StorageContext storageContext, Integracao integracao, EntityEntry entry)
        {
            IDictionary<string, object> state = Templating.ObjectToDictionary(entry.Entity);

            long id = (long) state["ID"];

            if(integracao.Campos != null){
                Formats.FilterFields(state, integracao.Campos);
            }

            var values = new Dictionary<string, string>(state.Count);
            foreach(var propertyName in new List<string>(state.Keys))
            {
                values[propertyName] = state[propertyName].ToString();
            }

            IntegracaoAtividade activity = new IntegracaoAtividade()
            {
                IntegracaoID = integracao.ID,
                Criacao = DateTime.Now,
                NomeEntidade = entry.Entity.GetType().Name,
                IdentificadorEntidade = id,

                HttpSentData = Formats.ToJson(state)

            };


            if(integracao.RequisicaoMetodo == "POST"){

                var content = new FormUrlEncodedContent(values);
                var response = client.PostAsync(integracao.RequisicaoUrl, content).GetAwaiter().GetResult();

                activity.HttpStatus = (int) response.StatusCode;
                activity.HttpResponse = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            } else

            if(integracao.RequisicaoMetodo == "GET"){

                var uriBuilder = new UriBuilder(integracao.RequisicaoUrl);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);

                foreach(var propertyName in new List<string>(state.Keys)){
                    query[propertyName] = values[propertyName];
                }
                
                uriBuilder.Query = query.ToString();

                var response = client.GetAsync(uriBuilder.ToString()).GetAwaiter().GetResult();

                activity.HttpStatus = (int) response.StatusCode;
                activity.HttpResponse = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            }

            return activity;
        }

    }

}