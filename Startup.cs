﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PessoaCrud.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using PessoaCrud.Services;

namespace PessoaCrud
{
    public class Startup
    {
        static readonly string SendGridApiKeyVariable = "SEND_GRID_API_KEY";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string sendGridApiKey = Environment.GetEnvironmentVariable(SendGridApiKeyVariable);

            if(sendGridApiKey == null){
                throw new InvalidOperationException("Missing " + SendGridApiKeyVariable + " enviroment variable.");
            }

            services.AddTransient<IntegrationDispatcher, IntegrationDispatcher>();
            services.AddTransient<WebCall, WebCall>();
            services.AddTransient<Mailing>(provider =>  new Mailing(sendGridApiKey)); 

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<StorageContext>(options =>
                options.UseInMemoryDatabase(databaseName:"database_name")
            );

            services.AddMvc(options => options.EnableEndpointRouting = false)
            .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            //app.UseCookiePolicy();
            app.UseRouting();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Pessoas}/{action=Index}/{id?}");
            });
        }
    }

}
